# Installationsanleitung

## Vorraussetzung
Folgende Programme werden zur Installation benötigt:
- `git`
- `conda`
- `yarn==1.22.19`
- `nodejs>=10.0.0`

Beachten sie, dass das `nodejs` Paket in aktuelleren Versionen für Ubuntu nur für folgende Architekturen
gebaut wird:
* amd64 (64-bit)
* armhf (ARM 32-bit hard-float, ARMv7 and up: arm-linux-gnueabihf)
* arm64 (ARM 64-bit, ARMv8 and up: aarch64-linux-gnu)

Für Redhat, Fedora wird es für diese Architekuren gebaut:
* x86_64 (64-bit)
* arm64 (ARM 64-bit, ARMv8 and up: aarch64-linux-gnu)

32-Bit Systeme wie werden nur bei Arm Prozessoren unterstützt. Stellen sie sicher, 
dass sie diese Anforderung erfüllen bevor sie fortfahren.
Andernfalls müssen sie das Paket selbst für ihre Architektur compilieren.

## Installation
Zuerst müssen sie das Projekt mit git herunterladen. Anschließend installieren sie die 
benötigte Python Umgebung mit dem `conda` Befehl. Zum Schluss müssen sie das Frontend
mit dem Befehl `yarn` bauen.

Führen sie dazu folgende Befehle der Reihe nach aus:
```shell
git clone git@git.informatik.uni-leipzig.de:ml-group/tools-dev/what-if-medical/what-if-medical-tool.git

# Set up Python environment
cd ~/what-if-medical-tool
conda env create -f environment.yml
conda activate lit-nlp

# Build the frontend
(cd lit_nlp; yarn && yarn build)
```

## Probleme während der Installation
Wenn ein [Fehler](https://github.com/yarnpkg/yarn/issues/2821) beim `yarn` Befehl auftritt, kann es an einer falschen Version liegen.
Aktualisieren sie `yarn` und versuchen sie es erneut. Getestet wurde das Projekt mit Version `1.22.19`.
Bitte beachten sie das diese `yarn` Version `nodejs` mit einer Version größer oder gleich `10.0.0` benötigt.
Wenn sie Ubuntu nutzen gibt es [hier](https://github.com/yarnpkg/yarn/issues/2821#issuecomment-284181365) eine Anleitung zur Aktualisierung von `yarn`.
Um `nodejs` auf Ubuntu zu aktualisieren können sie eine neuere Version entweder aus Fremdquellen oder
aus dem Snap Store installieren. Eine Installationsanleitung aus den Fremdquellen finden sie
[hier](https://github.com/nodesource/distributions#debian-and-ubuntu-based-distributions).

Wenn sie eine Fehler beim Ausführen von `yarn` sehen, der wie folgt aussieht:
```
Error: error:0308010C:digital envelope routines::unsupported
```
dann führen sie folgenden Befehl in ihrem Terminal aus und versuchen es erneut:
```shell
export NODE_OPTIONS=--openssl-legacy-provider
```


## Probleme nach der Installation

### Probleme mit dem protobuf Paket
Das Python-Paket `protobuf` kann Abhängigkeitsprobleme verursachen. Das Problem ist bekannt
und die Lösung ist in einem [Github Issue](https://github.com/PAIR-code/lit/issues/1148#issuecomment-1609122716) verlinkt.
Der Fehler tritt beim Starten der Demos auf und die zugehörige Fehlermeldung ist im Issue
angegeben.
Auf [Stackoverflow](https://stackoverflow.com/questions/71759248/importerror-cannot-import-name-builder-from-google-protobuf-internal/72494013#72494013) hat ein Nutzer eine Lösung vorgeschlagen.

Hier die beschriebene Prozedur aus dem Post:
1. Die aktuellste Version von `protobuf` installieren: `pip install --upgrade protobuf`
2. Kopieren sie die Datei `builder.py` von `.../Lib/site-packages/google/protobuf/internal` in ein anderes Verzeichnis
3. Installieren sie die mit ihrem Projekt kompatible `protobuf` Version: `pip install protobuf==3.19.6`
4. Kopieren sie die zwischengespeicherte Datei `builder.py` zurück in `.../Lib/site-packages/google/protobuf/internal`

### Testdaten erscheinen nicht
Nachdem sie den Webserver mit einem der Demos erfolgreich gestartet haben und die
Weboberfläche in ihren Browser geöffnet haben sollten sie eine Tabelle mit allen
Testdaten sehen können. Manchmal kommt es vor, dass die Tabelle leer ist und keine 
Daten geladen werden. In diesem Fall laden sie die Seite neu. Jetzt sollten die Tabelle
mit den Daten erscheinen.
