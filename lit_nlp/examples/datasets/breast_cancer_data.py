from sklearn.model_selection import train_test_split

from lit_nlp.api import dataset as lit_dataset
from lit_nlp.api import types as lit_types
import zipfile
import pandas as pd
import requests
import io

VOCABS = {
    "class": ["no-recurrence-events", "recurrence-events"],
    "age": ["10-19", "20-29", "30-39", "40-49", "50-59", "60-69", "70-79", "80-89", "90-99"],
    "menopause": ["lt40", "ge40", "premeno"],
    "tumor-size": ["0-4", "5-9", "10-14", "15-19", "20-24", "25-29", "30-34", "35-39", "40-44", "45-49", "50-54",
                   "55-59"],
    "inv-nodes": ["0-2", "3-5", "6-8", "9-11", "12-14", "15-17", "18-20", "21-23", "24-26", "27-29", "30-32", "33-35",
                  "36-39"],
    "node-caps": ["yes", "no"],
    "deg-malig": ["1", "2", "3"],
    "breast": ["left", "right"],
    "breast-quad": ["left_up", "left_low", "right_up", "right_low", "central"],
    "irradiat": ["yes", "no"]
}

DATAPATH = "https://archive.ics.uci.edu/static/public/14/breast+cancer.zip"
ATTRIBUTES = ['class', 'age', 'menopause', 'tumor-size', 'inv-nodes', 'node-caps', 'deg-malig', 'breast', 'breast-quad',
              'irradiat']


class BreastCancerDataset(lit_dataset.Dataset):

    def __init__(self):
        r = requests.get(DATAPATH, stream=True)
        zf = zipfile.ZipFile(io.BytesIO(r.content))
        df = pd.read_csv(zf.open('breast-cancer.data'), names=ATTRIBUTES)
        df = df[df['node-caps'] != '?']
        df = df[df['breast-quad'] != '?']
        _, test = train_test_split(df, shuffle=True, random_state=1, test_size=0.4)

        for rec in test.to_dict('records'):
            ex = {
                'class': rec['class'],
                'age': rec['age'],
                'menopause': rec['menopause'],
                'tumor-size': rec['tumor-size'],
                'inv-nodes': rec['inv-nodes'],
                'node-caps': rec['node-caps'],
                'deg-malig': rec['deg-malig'],
                'breast': rec['breast'],
                'breast-quad': rec['breast-quad'],
                'irradiat': rec['irradiat']
            }
            self._examples.append(ex)

    def spec(self):
        return {
            "class": lit_types.CategoryLabel(vocab=VOCABS['class']),
            'age': lit_types.CategoryLabel(vocab=VOCABS['age']),
            "menopause": lit_types.CategoryLabel(vocab=VOCABS["menopause"]),
            "tumor-size": lit_types.CategoryLabel(vocab=VOCABS["tumor-size"]),
            "inv-nodes": lit_types.CategoryLabel(vocab=VOCABS["inv-nodes"]),
            "node-caps": lit_types.CategoryLabel(vocab=VOCABS["node-caps"]),
            "deg-malig": lit_types.CategoryLabel(vocab=VOCABS["deg-malig"]),
            "breast": lit_types.CategoryLabel(vocab=VOCABS["breast"]),
            "breast-quad": lit_types.CategoryLabel(vocab=VOCABS["breast-quad"]),
            "irradiat": lit_types.CategoryLabel(vocab=VOCABS["irradiat"])
        }
