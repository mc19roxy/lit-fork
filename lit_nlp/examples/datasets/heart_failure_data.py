from lit_nlp.api import dataset as lit_dataset
from lit_nlp.api import types as lit_types
import zipfile
import pandas as pd
import requests
import io
from sklearn.model_selection import train_test_split

DATAPATH = "https://archive.ics.uci.edu/static/public/519/heart+failure+clinical+records.zip"

VOCABS = {
    'sex': ['female', 'male'],
    'death_event': ['dead', 'alive'],
    'bool': ['no', 'yes']
}


class HeartFailureDataset(lit_dataset.Dataset):

    def __init__(self):
        r = requests.get(DATAPATH, stream=True)
        zf = zipfile.ZipFile(io.BytesIO(r.content))
        df = pd.read_csv(zf.open('heart_failure_clinical_records_dataset.csv'))
        _, test = train_test_split(df, shuffle=False, test_size=0.4)

        for rec in test.to_dict('records'):
            ex = {
                'age': rec['age'],
                "anaemia": VOCABS['bool'][rec['anaemia']],
                "creatinine_phosphokinase": rec['creatinine_phosphokinase'],
                "diabetes": VOCABS['bool'][rec['diabetes']],
                "ejection_fraction": rec["ejection_fraction"],
                "high_blood_pressure": VOCABS['bool'][rec['high_blood_pressure']],
                "platelets": rec["platelets"],
                "serum_creatinine": rec["serum_creatinine"],
                "serum_sodium": rec["serum_sodium"],
                "sex": VOCABS['sex'][rec['sex']],
                "smoking": VOCABS['bool'][rec['smoking']],
                "time": rec["time"],
                "death_event": VOCABS['death_event'][rec["DEATH_EVENT"]]
            }
            self._examples.append(ex)

    def spec(self):
        return {
            "age": lit_types.Scalar(),
            "anaemia": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "creatinine_phosphokinase": lit_types.Scalar(),
            "diabetes": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "ejection_fraction": lit_types.Scalar(),
            "high_blood_pressure": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "platelets": lit_types.Scalar(),
            "serum_creatinine": lit_types.Scalar(),
            "serum_sodium": lit_types.Scalar(),
            "sex": lit_types.CategoryLabel(vocab=VOCABS['sex']),
            "smoking": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "time": lit_types.Scalar(),
            "death_event": lit_types.CategoryLabel(vocab=VOCABS['death_event'])
        }
