import zipfile
import pandas as pd
import requests
import io
import os

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.dummy import DummyClassifier
import pickle

DATAPATH = "https://archive.ics.uci.edu/static/public/14/breast+cancer.zip"
CSV_HEADER = ['class', 'age', 'menopause', 'tumor-size', "inv-nodes", "node-caps", "deg-malig", "breast", "breast-quad", "irradiat"]

VOCABS = {
    "class": ["no-recurrence-events", "recurrence-events"],
    "age": ["10-19", "20-29", "30-39", "40-49", "50-59", "60-69", "70-79", "80-89", "90-99"],
    "menopause": ["lt40", "ge40", "premeno"],
    "tumor-size": ["0-4", "5-9", "10-14", "15-19", "20-24", "25-29", "30-34", "35-39", "40-44", "45-49", "50-54",
                   "55-59"],
    "inv-nodes": ["0-2", "3-5", "6-8", "9-11", "12-14", "15-17", "18-20", "21-23", "24-26", "27-29", "30-32", "33-35",
                  "36-39"],
    "node-caps": ["yes", "no"],
    "deg-malig": ["1", "2", "3"],
    "breast": ["left", "right"],
    "breast-quad": ["left_up", "left_low", "right_up", "right_low", "central"],
    "irradiat": ["yes", "no"]
}

# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    csv_path = "/tmp/breast-cancer.data"
    df = None
    csv_file = None
    if os.path.exists(csv_path):
        print("found cached csv file")
        csv_file = open(csv_path, "r")
        df = pd.read_csv(csv_file, index_col=0)
    else:
        r = requests.get(DATAPATH, stream=True)
        zf = zipfile.ZipFile(io.BytesIO(r.content))
        csv_file = zf.open('breast-cancer.data')
        df = pd.read_csv(csv_file, names=CSV_HEADER)
        df = df[df['node-caps'] != '?']
        df = df[df['breast-quad'] != '?']
        df.to_csv(csv_path)

    train, test = train_test_split(df, shuffle=True, test_size=0.4, random_state=1)

    test.to_csv('breast_cancer_test.csv')

    for col in train.columns:
        encoded_list = []
        for entry in train[col]:
            encoded_list.append(VOCABS[col].index(str(entry)))
        train[col] = encoded_list

    print(df.head())

    print("test Datensatz: ", len(test))
    print("train Datensatz", len(train))

    target = train['class'].to_numpy()
    data = train.iloc[:, 1:].to_numpy()

    random_forest = RandomForestClassifier(n_estimators=50)
    random_forest.fit(data, target)

    decision_tree = DecisionTreeClassifier()
    decision_tree = decision_tree.fit(data, target)

    dummy_classifier = DummyClassifier(strategy="stratified")
    dummy_classifier.fit(data, target)

    s = pickle.dumps(decision_tree)
    with open("breast-cancer-models/tree.pickle", 'wb') as f:
        f.write(s)

    s = pickle.dumps(random_forest)
    with open("breast-cancer-models/random_forest.pickle", 'wb') as f:
        f.write(s)

    s = pickle.dumps(dummy_classifier)
    with open("breast-cancer-models/dummy_classifier.pickle", "wb") as f:
        f.write(s)





# See PyCharm help at https://www.jetbrains.com/help/pycharm/
