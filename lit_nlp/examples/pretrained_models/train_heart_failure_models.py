import zipfile
import pandas as pd
import requests
import io
import os

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.dummy import DummyClassifier
import pickle

DATAPATH = "https://archive.ics.uci.edu/static/public/519/heart+failure+clinical+records.zip"

# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    csv_path = "/tmp/heart_failure_clinical_records_dataset.csv"
    df = None
    csv_file = None
    if os.path.exists(csv_path):
        print("found cached csv file")
        csv_file = open(csv_path, "r")
        df = pd.read_csv(csv_file)
    else:
        r = requests.get(DATAPATH, stream=True)
        zf = zipfile.ZipFile(io.BytesIO(r.content))
        csv_file = zf.open('heart_failure_clinical_records_dataset.csv')
        df = pd.read_csv(csv_file)
        df.to_csv(csv_path)

    train, test = train_test_split(df, shuffle=False, test_size=0.4)
    print("test Datensatz: ", len(test))
    print("train Datensatz", len(train))
    data = train.to_numpy()[:, 1:]
    target = data[:, -1]
    data = data[:, :-1:]

    print("features: ", len(data[0]))

    random_forest = RandomForestClassifier(n_estimators=50)
    random_forest.fit(data, target)

    decision_tree = DecisionTreeClassifier()
    decision_tree = decision_tree.fit(data, target)

    dummy_classifier = DummyClassifier(strategy="stratified")
    dummy_classifier.fit(data, target)

    test_data = test.to_numpy()[:, 1:][1]
    print(test_data)
    test_target = test_data[-1::]
    test_data = test_data[:-1:]
    print('len of test data', len(test_data))

    print("target: ", test_target)
    print("tree prediction: ", decision_tree.predict([test_data]))
    print("tree prediction probs: ", decision_tree.predict_proba([test_data]))

    print("forest prediction: ", random_forest.predict([test_data]))
    print("forest prediction probs: ", random_forest.predict_proba([test_data]))

    print("dummy prediction: ", dummy_classifier.predict([test_data]))
    print("dummy prediction probs: ", dummy_classifier.predict_proba([test_data]))

    s = pickle.dumps(decision_tree)
    with open("decision_tree.pickle", 'wb') as f:
        f.write(s)

    s = pickle.dumps(random_forest)
    with open("random_forest.pickle", 'wb') as f:
        f.write(s)

    s = pickle.dumps(dummy_classifier)
    with open("dummy_classifier.pickle", "wb") as f :
        f.write(s)





# See PyCharm help at https://www.jetbrains.com/help/pycharm/
