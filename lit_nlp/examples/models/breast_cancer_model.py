import pickle
from typing import List
import numpy as np

from lit_nlp.api import model as lit_model
from lit_nlp.api import types as lit_types
from lit_nlp.api.model import JsonDict
from lit_nlp.examples.datasets.breast_cancer_data import VOCABS


class BreastCancerModel(lit_model.Model):

    def __init__(self, path):
        with open(path, mode='rb') as f:
            self.model = pickle.load(f)
    def input_spec(self):
        return {
            'age': lit_types.CategoryLabel(vocab=VOCABS['age']),
            "menopause": lit_types.CategoryLabel(vocab=VOCABS["menopause"]),
            "tumor-size": lit_types.CategoryLabel(vocab=VOCABS["tumor-size"]),
            "inv-nodes": lit_types.CategoryLabel(vocab=VOCABS["inv-nodes"]),
            "node-caps": lit_types.CategoryLabel(vocab=VOCABS["node-caps"]),
            "deg-malig": lit_types.CategoryLabel(vocab=VOCABS["deg-malig"]),
            "breast": lit_types.CategoryLabel(vocab=VOCABS["breast"]),
            "breast-quad": lit_types.CategoryLabel(vocab=VOCABS["breast-quad"]),
            "irradiat": lit_types.CategoryLabel(vocab=VOCABS["irradiat"])
        }

    def output_spec(self):
        return {
            'predicted_class':
                lit_types.MulticlassPreds(
                    parent='class', vocab=VOCABS['class'])
        }

    def predict_minibatch(self, inputs: List[JsonDict]) -> List[JsonDict]:
        def convert_inp(inp):
            a = np.array([VOCABS['age'].index(inp['age']), VOCABS['menopause'].index(inp['menopause']), VOCABS['tumor-size'].index(inp['tumor-size']),
                          VOCABS['inv-nodes'].index(inp['inv-nodes']), VOCABS['node-caps'].index(inp['node-caps']),
                          VOCABS['deg-malig'].index(str(inp['deg-malig'])), VOCABS['breast'].index(inp['breast']), VOCABS['breast-quad'].index(inp['breast-quad']),
                          VOCABS['irradiat'].index(inp['irradiat'])])
            return a.tolist()

        adjusted_inputs = [convert_inp(inp) for inp in inputs]
        model_outputs = [self.model.predict_proba([inp])[0] for inp in adjusted_inputs]
        ret = [{"predicted_class": out} for out in model_outputs]
        return ret

