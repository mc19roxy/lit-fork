import pickle
from typing import List

from lit_nlp.api import model as lit_model
from lit_nlp.api import types as lit_types
from lit_nlp.app import JsonDict
import numpy as np

from lit_nlp.examples.datasets.heart_failure_data import VOCABS


class HeartFailureModel(lit_model.Model):
    def __init__(self, path):
        with open(path, mode="rb") as f:
            self.model = pickle.load(f)

    def predict_minibatch(self, inputs: List[JsonDict]) -> List[JsonDict]:
        def convert_inp(inp):
            a = np.array([inp['age'], VOCABS['bool'].index(inp['anaemia']), inp['creatinine_phosphokinase'], VOCABS['bool'].index(inp['diabetes']),
                          inp['ejection_fraction'],
                          VOCABS['bool'].index(inp['high_blood_pressure']), inp['platelets'], inp['serum_creatinine'],
                          inp['serum_sodium'],
                          VOCABS['sex'].index(inp['sex']), VOCABS['bool'].index(inp['smoking']), inp['time']])
            return a.tolist()

        adjusted_inputs = [convert_inp(inp) for inp in inputs]
        model_outputs = [self.model.predict_proba([inp])[0] for inp in adjusted_inputs]
        ret = [{"predicted_death_event": out} for out in model_outputs]
        return ret

    def input_spec(self):
        return {
            "age": lit_types.Scalar(),
            "anaemia": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "creatinine_phosphokinase": lit_types.Scalar(),
            "diabetes": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "ejection_fraction": lit_types.Scalar(),
            "high_blood_pressure": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "platelets": lit_types.Scalar(),
            "serum_creatinine": lit_types.Scalar(),
            "serum_sodium": lit_types.Scalar(),
            "sex": lit_types.CategoryLabel(vocab=VOCABS['sex']),
            "smoking": lit_types.CategoryLabel(vocab=VOCABS['bool']),
            "time": lit_types.Scalar(),
        }

    def output_spec(self):
        return {
            'predicted_death_event':
                lit_types.MulticlassPreds(
                    parent="death_event", vocab=VOCABS['death_event'])
        }
