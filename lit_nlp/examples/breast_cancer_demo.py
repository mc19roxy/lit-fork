from typing import Sequence, Optional

from absl import flags, app

from lit_nlp import dev_server, server_flags
from lit_nlp.examples.datasets import breast_cancer_data
from lit_nlp.examples.models import breast_cancer_model

TREE_MODEL_PATH = "lit_nlp/examples/pretrained_models/breast_cancer/tree.pickle"
FOREST_MODEL_PATH = "lit_nlp/examples/pretrained_models/breast_cancer/random_forest.pickle"
DUMMY_MODEL_PATH = "lit_nlp/examples/pretrained_models/breast_cancer/dummy_classifier.pickle"

import transformers
TREE_MODEL_PATH = transformers.file_utils.cached_path(TREE_MODEL_PATH)
FOREST_MODEL_PATH = transformers.file_utils.cached_path(FOREST_MODEL_PATH)
DUMMY_MODEL_PATH = transformers.file_utils.cached_path(DUMMY_MODEL_PATH)

FLAGS = flags.FLAGS
_TREE_MODEL_PATH = flags.DEFINE_string('tree_model_path', TREE_MODEL_PATH,
                                  'Path to load trained model.')
_FOREST_MODEL_PATH = flags.DEFINE_string('forest_model_path', FOREST_MODEL_PATH,
                                  'Path to load trained model.')
_DUMMY_MODEL_PATH = flags.DEFINE_string('dummy_model_path', DUMMY_MODEL_PATH,
                                        'Path to load trained model.')

def main(argv: Sequence[str]) -> Optional[dev_server.LitServerType]:
    if len(argv) > 1:
        raise app.UsageError('Too many command-line arguments.')

    models = {
                'decision tree classifier': breast_cancer_model.BreastCancerModel(_TREE_MODEL_PATH.value),
                'random forest classifier': breast_cancer_model.BreastCancerModel(_FOREST_MODEL_PATH.value),
                'stratified dummy classifier': breast_cancer_model.BreastCancerModel(_DUMMY_MODEL_PATH.value)
              }
    datasets = {'breast_cancer': breast_cancer_data.BreastCancerDataset()}

    lit_demo = dev_server.Server(
        models,
        datasets,
        **server_flags.get_flags())
    return lit_demo.serve()


if __name__ == '__main__':
    app.run(main)