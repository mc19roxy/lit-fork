# Allgemeine Informationen
Das Projekt soll Medizinstudenten unterstützen maschinelle Lernverfahren besser
zu verstehen. Das hier entwickelte Tool visualisiert Datensätze und darauf trainierte 
Modelle in einer übersichtlichen Weboberfläche. Der gesamte Testdatensatz wird tabellarisch 
angezeigt. Die Entscheidungen des trainierten Modells zu einem Eintrag können einzeln abgefragt und mit 
dem Zielwert verglichen werden. Das Tool visualisiert auch Metriken über die Leistung des Modells wie Accuracy, Recall
und Precision. Einzelne Featurewerte bei einem Eintrag können verändert werden und dessen
Einfluss auf die Leistung des Modells analysiert werden.

Als Grundlage für die Entwicklung dieses Tools dient [lit](https://pair-code.github.io/lit/).
Diese Software analysiert und visualisiert Datensätze und trainierte Modelle.
Für mehr Informationen lesen sie das [Paper](https://doi.org/10.48550/arXiv.2008.05122) über das Tool oder besuchen sie die Github
Seite um den [Quellcode](https://github.com/PAIR-code/lit/) einzusehen. Die Software ist quelloffen und
mit der _Apache Lisense Version 2.0_ lizensiert.

# Datensätze
Wir verwenden 2 Datensätze aus dem medizinischen Kontext.

|             | Url    | Anzahl Features | Größe | Lizenz | Urheber                        | Paper | Zielwert |
|-------------|--------|-----------------|-------|--------|--------------------------------|-------|----------|
| Brustkrebs  | https://archive.ics.uci.edu/dataset/14/breast+cancer | 9               |    286   |    Creative Commons Attribution 4.0 International (CC BY 4.0)     | Matjaz Zwitter, Milan Soklic   |       |    Class      |
| Herzinfarkt | https://archive.ics.uci.edu/dataset/519/heart+failure+clinical+records | 13              | 299   |    Creative Commons Attribution 4.0 International (CC BY 4.0)    | Davide Chicco, Giuseppe Jurman |    "Machine learning can predict survival of patients with heart failure from serum creatinine and ejection fraction alone". BMC Medical Informatics and Decision Making 20, 16 (2020). https://doi.org/10.1186/s12911-020-1023-5    |     death event: if the patient died during the follow-up period     |

# Modelle
Für beide Datensätze haben wir Modelle mit 3 verschiedenen [Scipy](https://scipy.org/)-ML-Verfahren trainiert:
1. [Stratisfied Dummy Classifier](https://scikit-learn.org/stable/modules/model_evaluation.html#dummy-estimators)
2. [Decision Tree](https://scikit-learn.org/stable/modules/tree.html#tree)
3. [Random Forest](https://scikit-learn.org/stable/modules/ensemble.html#random-forests)

Die Datensätze wurden dazu in 60% Trainingsdaten und 40% Testdaten aufgeteilt.

# Starten der Demos
Mit den beiden Datensätzen können sie 2 Demos starten. Dabei wird ein Webserver
lokal auf dem Rechner gestartet, 
der die Weboberfläche von `lit` bereitstellt und den Testdatensatz sowie die Modelle lädt.
Die Oberfläche können sie anschließend in einem Webbrowser ihrer Wahl aufrufen.
Führen sie dazu folgende Befehle aus.

Für die Brustkrebsdemo:
```shell
python -m lit_nlp.examples.breast_cancer_demo --port=5432 
```
Für die Herzinfarktdemo:
```shell
 python -m lit_nlp.examples.heart_failure_demo --port=5432
```
Wenn sie beide Demos gleichzeitig starten möchten, müssen sie unterschiedliche Ports
angeben. Ansonsten kommt es zu einem Konflikt.

Öffnen sie anschließend ihren Browser und navigieren sie zu der Adresse: `localhost:5432`.
Passen sie die Adresse an, falls sie die Demo unter einem anderen Port gestartet haben.
